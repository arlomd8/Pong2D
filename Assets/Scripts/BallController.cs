﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BallController : MonoBehaviour
{

    public int force;
    Rigidbody2D rigid;
    int scoreP1;
    int scoreP2;
    Text scoreUIP1;
    Text scoreUIP2;
    GameObject panelSelesai;
    Text txPemenang;

    private float timer;
    private bool canCount = true;
    private bool doOnce = false;
    private bool timerStart = false;
    private float time;
    private bool timesUp = false;

    private bool AsetDestroyed;

    Text timerText;
    Text bintang;
    Text scoreText;

    int jmlBintang;
    float score;
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        Vector2 arah = new Vector2(2, 0).normalized;
        rigid.AddForce(arah * force);

        scoreP1 = 0;
        scoreP2 = 0;
        scoreUIP1 = GameObject.Find("Score1").GetComponent<Text>();
        scoreUIP2 = GameObject.Find("Score2").GetComponent<Text>();
        panelSelesai = GameObject.Find("PanelSelesai");
        panelSelesai.SetActive(false);

        time = 30;
        timerStart = true;
        timerText = GameObject.Find("Timer").GetComponent<Text>();
        jmlBintang = 0;
        score = 0;

    }

    // Update is called once per frame
    void Update()
    {
        timerText.text = time.ToString();
        if (time >= 0)
        {
            time -= Time.deltaTime;
        }
        if (time <= 0)
        {
            time = 0;
            timesUp = true;
            panelSelesai.SetActive(true);

            if (scoreP1 > scoreP2)
            {
                txPemenang = GameObject.Find("Pemenang").GetComponent<Text>();
                txPemenang.text = "BIRU MENANG";
                bintang = GameObject.Find("Bintang").GetComponent<Text>();
                scoreText = GameObject.Find("Trophy").GetComponent<Text>();
                CheckWin();
                Destroy(gameObject);
                return;

            }

            if (scoreP1 == scoreP2)
            {
                txPemenang = GameObject.Find("Pemenang").GetComponent<Text>();
                txPemenang.text = "DRAW";
                bintang = GameObject.Find("Bintang").GetComponent<Text>();
                bintang.text = "BINTANG x 0";
                scoreText = GameObject.Find("Trophy").GetComponent<Text>();
                scoreText.text = "SCORE : 0";
                Destroy(gameObject);
                return;
            }

            else
            {
                txPemenang = GameObject.Find("Pemenang").GetComponent<Text>();
                txPemenang.text = "MERAH MENANG";
                bintang = GameObject.Find("Bintang").GetComponent<Text>();
                scoreText = GameObject.Find("Trophy").GetComponent<Text>();
                CheckWin();
                Destroy(gameObject);
                return;

            }
        }

    }



    private void OnCollisionEnter2D(Collision2D coll)
    {

        if (coll.gameObject.name == "AsetMerah")
        {
            scoreP1 += 2;
            TampilkanScore();
            CheckWin();
            Destroy(gameObject);
            return;
        }
        if (coll.gameObject.name == "AsetBiru")
        {
            scoreP2 += 2;
            TampilkanScore();
            CheckWin();
            Destroy(gameObject);
            return;
        }

        if (coll.gameObject.name == "Pemukul1" || coll.gameObject.name == "Pemukul2")
        {
            float sudut = (transform.position.y - coll.transform.position.y) * 5f;
            Vector2 arah = new Vector2(rigid.velocity.x, sudut).normalized;
            rigid.velocity = new Vector2(0, 0);
            rigid.AddForce(arah * force * 2);
        }

        if (coll.gameObject.name == "TembokMerah1" || coll.gameObject.name == "TembokMerah2" ||
            coll.gameObject.name == "TembokMerah3")
        {
            float sudut = (transform.position.y - coll.transform.position.y) * 5f;
            Vector2 arah = new Vector2(rigid.velocity.x, sudut).normalized;
            rigid.velocity = new Vector2(0, 0);
            rigid.AddForce(arah * force * 2);
            scoreP1 += 1;
            TampilkanScore();

        }

        if (coll.gameObject.name == "TembokBiru1" ||
           coll.gameObject.name == "TembokBiru2" || coll.gameObject.name == "TembokBiru3")
        {
            float sudut = (transform.position.y - coll.transform.position.y) * 5f;
            Vector2 arah = new Vector2(rigid.velocity.x, sudut).normalized;
            rigid.velocity = new Vector2(0, 0);
            rigid.AddForce(arah * force * 2);
            scoreP2 += 1;
            TampilkanScore();

        }


    }
    void ResetBall()
    {
        transform.localPosition = new Vector2(0, 0);
        rigid.velocity = new Vector2(0, 0);
    }

    void CheckWin()
    {
        panelSelesai.SetActive(true);
        txPemenang = GameObject.Find("Pemenang").GetComponent<Text>();
        bintang = GameObject.Find("Bintang").GetComponent<Text>();
        scoreText = GameObject.Find("Trophy").GetComponent<Text>();
        if (scoreP1 == 1)  //bintang 1
        {
            print("Bintang 1");
            jmlBintang = 1;
            score = 1 / 3f * 100f;
            txPemenang.text = "BIRU MENANG";
            bintang.text = "⭐ x " + jmlBintang.ToString();
            scoreText.text = "SCORE : " + score.ToString();
        }

        if (scoreP1 == 3) //bintang 2
        {
            print("Bintang 2");
            jmlBintang = 2;
            score = 2 / 3f * 100f;
            print(score);
            txPemenang.text = "BIRU MENANG";
            bintang.text = "⭐ x " + jmlBintang.ToString();
            scoreText.text = "SCORE : " + score.ToString();

        }
        if (scoreP1 == 4) //bintang 3
        {
            print("Bintang 3");
            jmlBintang = 3;
            score = 3 / 3f * 100f;
            txPemenang.text = "BIRU MENANG";
            bintang.text = "⭐ x " + jmlBintang.ToString();
            scoreText.text = "SCORE : " + score.ToString();

        }
        if (scoreP1 == 5) //bintang 3
        {
            print("Bintang 3");
            jmlBintang = 3;
            score = 3 / 3f * 100f - 20f;
            txPemenang.text = "BIRU MENANG";
            bintang.text = "⭐ x " + jmlBintang.ToString();
            scoreText.text = "SCORE : " + score.ToString();

        }

        if (scoreP2 == 1)  //bintang 1
        {
            print("Bintang 1");
            jmlBintang = 1;
            score = 1 / 3f * 100f;
            txPemenang.text = "MERAH MENANG";
            bintang.text = "⭐ x " + jmlBintang.ToString();
            scoreText.text = "SCORE : " + score.ToString();
        }

        if (scoreP2 == 3) //bintang 2
        {
            print("Bintang 2");
            jmlBintang = 2;
            score = 2 / 3f * 100f;
            txPemenang.text = "MERAH MENANG";
            bintang.text = "⭐ x " + jmlBintang.ToString();
            scoreText.text = "SCORE : " + score.ToString();

        }

        if (scoreP2 == 4) //bintang 3
        {
            print("Bintang 3");
            jmlBintang = 3;
            score = 3 / 3f * 100f;
            txPemenang.text = "MERAH MENANG";
            bintang.text = "⭐ x " + jmlBintang.ToString();
            scoreText.text = "SCORE : " + score.ToString();

        }
        if (scoreP2 == 5) //bintang 3
        {
            print("Bintang 3");
            jmlBintang = 3;
            score = 3 / 3f * 100f - 20f;
            txPemenang.text = "MERAH MENANG";
            bintang.text = "⭐ x " + jmlBintang.ToString();
            scoreText.text = "SCORE : " + score.ToString();

        }


    }

    void TampilkanScore()
    {
        //Debug.Log("Score P1: " + scoreP1 + "Score P2: " + scoreP2);
        scoreUIP1.text = scoreP1 + "";
        scoreUIP2.text = scoreP2 + "";
       
    }

}



