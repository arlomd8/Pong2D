﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TembokScript : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name == "Bola")
        {
            Destroy(gameObject);
        }
    }
}
